#pragma once

#include <string>
#include <map>

class morse_encoder {

public:
    morse_encoder() = default;
    morse_encoder(const std::string& text);
    ~morse_encoder();
    
    void set_text(const std::string& text);
    const std::string& get_text() const;
    std::string encode() const;

private:
    typedef std::map<char,std::string> morse_map;

    std::string _text;    
    static morse_map _map; 

};
