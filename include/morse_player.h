#pragma once
#include <string>

class morse_player {
public:
    morse_player() = default;
    morse_player(const std::string& code);
    ~morse_player();

    void set_code(const std::string& code);
    const std::string& get_code() const;

    void play() const;
private:
    std::string _code;
    static u_int32_t _dot_time;

    static void play_char(char c);
    static void play_dot();
    static void play_dash();
    static void play_space();
    static void play_slash();

};

