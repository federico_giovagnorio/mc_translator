#include <iostream>
#include <string>
#include <unistd.h>
#include <stdexcept>

#include "include/morse_player.h"

using namespace std;

morse_player::morse_player(const string& code) {
    _code = code;
}

morse_player::~morse_player() {

}

void morse_player::set_code(const std::string& code) {
    _code = code;
}

const std::string& morse_player::get_code() const {
    return _code;
}

void morse_player::play() const {
    for (char c : _code) {
        try {
            morse_player::play_char(c);
        } catch (exception& e) {
            throw;
        }
    }
}

void morse_player::play_char(char c){
    switch (c) {
        case '.' :
            morse_player::play_dot();
            break;
        case '-' :
            morse_player::play_dash();
            break;
        case ' ' :
            morse_player::play_space();
            break;
        case '/' :
            morse_player::play_slash();
            break;
        default :
            throw invalid_argument { "'" + string(1,c) + "' not in {., -,  , /}" };
            break;
    }
}

void morse_player::play_dot(){
    system("paplay sounds/dot_60.wav");
    usleep((_dot_time * 1000));
}

void morse_player::play_dash(){
    system("paplay sounds/dash_180.wav");
    usleep((_dot_time * 1000));
}

void morse_player::play_space(){
    usleep(_dot_time * 3000);
}

void morse_player::play_slash(){
    usleep(_dot_time * 7000);
}

u_int32_t morse_player::_dot_time { 60 };