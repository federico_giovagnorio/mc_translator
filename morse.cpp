#include <iostream>
#include <string>
#include <stdexcept>

#include "include/morse_encoder.h"
#include "include/morse_player.h"

using namespace std;

string user_input { };
string code { };
morse_encoder encoder { };
morse_player player { };

void routine() {
    cout << ">> ";
    getline(cin, user_input);
    
    encoder.set_text(user_input);
    try {
        code = encoder.encode();
        cout << code << endl;
    } catch (exception& e) {
        cerr << e.what() << endl;
        return;
    }

    player.set_code(code);
    try {
        player.play();
    } catch (exception& e) {
        cerr << e.what() << endl;
        return;
    }

}

int main() {
    while (true) {
        routine();
    }   
}