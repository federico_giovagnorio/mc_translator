CC=g++
RM = rm -rf
CFLAGS=-g -Wall

.PRECIOUS: %.o #elimina file .o
vpath %.h include/

OBJ=morse.o morse_encoder.o morse_player.o
TARGET=morse

default: $(TARGET)

morse: morse.o morse_encoder.o morse_player.o 
	$(CC) $(CFLAGS) -o morse morse.o morse_encoder.o morse_player.o

morse.o: morse.cpp morse_encoder.h morse_player.h
	$(CC) $(CFLAGS) -c morse.cpp

morse_encoder.o: morse_encoder.cpp morse_encoder.h
	$(CC) $(CFLAGS) -c morse_encoder.cpp

morse_player.o: morse_player.cpp morse_player.h
	$(CC) $(CFLAGS) -c morse_player.cpp

clean:
	$(RM) $(TARGET) $(OBJ)