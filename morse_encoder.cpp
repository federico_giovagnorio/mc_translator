#include <string>
#include <map>
#include <cwctype>
#include <cctype>
#include <stdexcept>

#include "include/morse_encoder.h"

using namespace std;

morse_encoder::morse_encoder(const string& text) {
    _text = text; 
}

morse_encoder::~morse_encoder() {

}

void morse_encoder::set_text(const std::string& text){
    _text = text;
}

const std::string& morse_encoder::get_text() const{
    return _text;
}

string morse_encoder::encode() const {
    string enc_text { };

    for (char c : _text) {
        if (c != ' ' && !iswalnum(c))
            throw invalid_argument { "'" + string(1,c) + "' not in {A-Z,0-9}" };
        if (c == ' ' && enc_text.back() == '/')
            continue;
        enc_text += c == ' ' ? "/ " : _map[toupper(c)] + " ";
    }

    while(enc_text[0] == '/' || enc_text[0] == ' ')
        enc_text.erase(enc_text.begin());
    while(enc_text.back() == '/' || enc_text.back() == ' ')
        enc_text.erase(enc_text.end() - 1);
    
    return enc_text;
}

morse_encoder::morse_map morse_encoder::_map { 
    {
    {'A', ".-"},
    {'B', "-..."},
    {'C', "-.-."},
    {'D', "-.."},
    {'E', "."},
    {'F', "..-."},
    {'G', "--."},
    {'H', "...."},
    {'I', ".."},
    {'J', ".---"},
    {'K', "-.-"},
    {'L', ".-.."},
    {'M', "--"},
    {'N', "-."},
    {'O', "---"},
    {'P', ".--."},
    {'Q', "--.-"},
    {'R', ".-."},
    {'S', "..."},
    {'T', "-"},
    {'U', "..-"},
    {'V', "...-"},
    {'W', ".--"},
    {'X', "-..-"},
    {'Y', "-.--"},
    {'Z', "--.."},
    {'1', ".----"},
    {'2', "..---"},
    {'3', "...--"},
    {'4', "....-"},
    {'5', "....."},
    {'6', "-...."},
    {'7', "--..."},
    {'8', "---.."},
    {'9', "----."},
    {'0', "-----"} 
    }
};